@extends('layouts.main')
@section('title','Tambah Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Tambah Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('datas') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body">
                     <div class="row">
                         <div class="col-md-6 offset-md-3">
                             <form action="{{ url('datas') }}" method="post">
                                @csrf
                                
                                 <div class="form-group">
                                     <label >Nama Karyawan</label>
                                     <input type="text" name="nama" class="form-control" autofocus required>
                                 </div>
                                 <div class="form-group">
                                    <label >No Karyawan</label>
                                    <input type="text" name="no" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >No Tlp Karyawan</label>
                                    <input type="text" name="notlp" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Jabatan Karyawan</label>
                                    <input type="text" name="jabatan" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Divisi Karyawan</label>
                                    <input type="text" name="divisi" class="form-control" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection