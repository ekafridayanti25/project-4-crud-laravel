@extends('layouts.main')
@section('title','Edit Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Edit Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('datas') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                     <div class="row">
                         <div class="col-md-6  offset-md-3">
                             <form action="{{ url('datas/edit/'.$datas->id) }}" method="post">
                                @method('put')
                                @csrf
                                 <div class="form-group">
                                     <label >Nama Karyawan</label>
                                     <input type="text" name="nama" class="form-control" value="{{ $datas->nama_karyawan }}" autofocus required>
                                 </div>
                                 <div class="form-group">
                                    <label >No Karyawan</label>
                                    <input type="text" name="no" id="no" class="form-control" value="{{ $datas->no_karyawan }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >No Tlp Karyawan</label>
                                    <input type="text" name="notlp" class="form-control" value="{{ $datas->no_tlp_karyawan }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Jabatan Karyawan</label>
                                    <input type="text" name="jabatan" class="form-control" value="{{ $datas->jabatan_karyawan }}"autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Divisi Karyawan</label>
                                    <input type="text" name="divisi" class="form-control" value="{{ $datas->divisi_karyawan }}" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-success">Update</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection